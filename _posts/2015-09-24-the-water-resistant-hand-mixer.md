---
layout: post
title: The Water-Resistant Hand Mixer
date: 2015-09-24 09:03:34 -0500
---
About a year ago, we purchased this hand mixer, and since then, this thing has proven itself to be more reliable than others and has taken a lot of abuse that probably would have killed many other mixers. It has fallen in both pancake batter and in a mixture of peanut butter, banana puree, and cocoa. Both times we rinsed it out with water and let it dry on a fan. This was probably not good for an electrical appliance, but it was better than throwing it away. Fortunately, we haven't electrocuted ourselves, and the mixer works as well as it did when we got it. It is very powerful and can cut through both cold butter and cookie dough. It is definitely better than the Hamilton Beach mixer we had been using.

Unfortunately, it does not come with a whisk attachment, but you can order it separately. Be forewarned though, this whisk attachment does fling what you are trying to mix more than others do. When using the whisk attachment, make sure you hold the mixer completely upright in order to minimize the mess.

This mixer is the Cuisinart 3-Speed Hand Mixer and costs $30 on Amazon. [Buy Now](http://www.amazon.com/dp/B00064NDY0/?tag=ccoequippilot-20)
![alt](/content/images/2015/09/61atGfw-AkL-_SL1500_.jpg)
