---
layout: post
title: Pumpkin Whoopie Pies
date: 2015-12-03 14:01:46 -0600
---
![alt](/content/images/2015/11/unnamed-copy-2.jpg)

This recipe has a very interesting history. It began with a lousy two-ingredient pumpkin muffin recipe (cake mix and pumpkin). With no spices it would have been very bland. So Mom added spices, then a stick of butter, and finally, after being inspired by a recipe from one of Dad’s coworkers, it evolved into pumpkin butterscotch pecan muffins. Since these muffins were half dessert and half breakfast, we decided to transform them into a complete dessert by making them into whoopie pies. We did this by fashioning the recipe first into cookies and then constructing them into sandwiches by adding some cream cheese frosting.

#### The Recipe

> - 1 (18.25 oz). yellow cake mix<sup>[1](#yellow-cake-note)</sup>
> - 94g chilled browned butter<sup>[2](#browned-butter-note)</sup> or ½ c. regular butter
> - 15 oz. butternut squash or canned pumpkin
> - 1 T. cinnamon
> - ½ tsp. nutmeg (freshly grated is always best)
> - ½ tsp. cloves
> - 1 c. butterscotch chips
> - ½ c. toasted pecans<sup>[3](#pecan-toasting-note)</sup>

Whisk spices and cake mix together. Using your fingers or a food processor, incorporate the cake mix and spices into the butter until it resembles coarse crumbs. Add the squash and beat with an electric mixer until nice and fluffy. Fold in the butterscotch chips and toasted pecans.

Using a #60 scoop (just a little over a tablespoon), place batter scoops on a parchment-lined baking stone. If desired, flatten<sup>[4](#flattening-note)</sup> scoops slightly. Bake at 350. The first batch will take about 10 to 12 minutes, or until a toothpick comes out clean. Avoid overbaking. Allow to rest for just a few minutes on the stone, then slide parchment off onto a cooling rack. Subsequent batches will take less time if the stone is not allowed to completely cool off. 1 full recipe will make about 32 to 36 sandwiches (64 to 72 cookie-cake plops).

#### Frosting/Filling:

> - 6 oz. cream cheese
> - 94g chilled browned butter<sup>[2](#browned-butter-note)</sup>
> - ½ tsp. vanilla               
> - 375g powdered sugar

Beat browned butter and cream cheese until fluffy. Add vanilla and powdered sugar and beat until good and fluffy again.

Each whoopie pie will get one scant #60 scoop of filling. No need to spread, just gently press another cake-cookie on top and the frosting should fill out nearly to edges. (If you have flattened the batter plops before baking, be really careful when pressing down as they are a bit more fragile when flatter).

Store at room temp if using within 24 hours, or store in the fridge for up to a week. BUT, for best flavor and texture, allow a couple of hours at least to get to room temp before eating—much better.

#### Notes:

<sup><a name="yellow-cake-note">1</a></sup> **Cake Mix:** We use the Hy-Vee brand since it is the only one we have found that still uses the 18.25 oz. size. Since cake mixes have gotten smaller here lately, I wouldn’t recommend using the smaller 15.25 oz. (Pillsbury and Betty Crocker) or 16.5 oz. (Duncan Hines) even though the manufacturers claim that they do the same thing.

<sup><a name="browned-butter-note">2</a></sup> **Browned Butter:** You can use regular butter, but we prefer browned. If you don’t have a scale, one stick of browned butter is about 94g. [Here’s how to make it:](http://joythebaker.com/2015/03/how-to-brown-butter/)  (we usually brown ours a bit more than she does, but you don’t want to burn it.) For this recipe and for the cream cheese frosting, you will want to chill and then slightly soften the butter.

<sup><a name="pecan-toasting-note">3</a></sup> **Pecan Toasting:** Bake as many pecans as you want on a rimmed baking sheet at 350 degrees for eight minutes, making sure to stir them halfway through. Ovens may vary, so be careful not to burn them. It may take more than or less than 8 minutes. Once they are done they should be aromatic, browned, and have a toasted flavor. Extra toasted pecans can be stored in the fridge.

<sup><a name="flattening-note">4</a></sup> **Flattening:** If eating the whoopie pies fresh, flattening the batter scoops isn’t necessary, especially since it makes it more difficult to squish the frosting in between the cookie-cakes because they aren’t as sturdy. However, if needing to store them in the fridge, they do turn out better if you flatten them some before baking. To do so, lightly grease a long strip of waxed paper and lay it greased-side down on top of the batter plops. Press down lightly to barely flatten them.
