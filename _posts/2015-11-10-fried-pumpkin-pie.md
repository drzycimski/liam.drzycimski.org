---
layout: post
title: Fried Pumpkin Pie
date: 2015-11-10 07:51:32 -0600
---
![alt](/content/images/2015/11/unnamed-1.jpg)

It’s November and the season for county fairs is over, but the memories aren’t gone yet. Oh no, I’m not talking about what you saw but what you ate. Everyone has their favorites. Last year, Dad found a place selling fried pumpkin pies that he really liked. It wasn’t until this year that we got around to replicating them.

When Dad first discovered fried pumpkin pies, we didn’t even consider making them because we thought getting the filling to stay in was some factory’s trick. Think of it- wouldn’t the filling just leak out, or not bake properly because of how runny it is? But after we did some looking on the internet, we found out that you can actually bake the filling first. Another method is to use just pumpkin, sugar, and spices. This didn’t sound as appealing as a regular pumpkin pie filling that involves mixing in dairy and eggs to form a custard. Following the method of baking the filling first, we simply used our own favorite filling and crust recipes.

You can freeze the pies before frying, which is nice because making them can become a bit time consuming. After freezing them on a baking sheet, wrap them in foil for further storage. When thawing, unwrap the foil and wrap them loosely in parchment paper, which will keep the pies from getting too soggy or too dry.

Although these are at their best if eaten right away, they are still amazing several hours later. We haven’t experimented with leaving them for the next day, but who would leave a pie around for that long?

Crust:
7½ oz. flour, then 5 oz added later
⅝ teaspoon table salt
2 tablespoons sugar
12 tablespoons cold salted butter (1 ½ sticks), cut into 1/4-inch slices
1/2 cup (3 oz.) vegetable shortening, cold, cut into 4 pieces
1/4 cup vodka, cold
1/4 cup cold water

Process 7½ oz. flour, salt, and sugar in food processor until combined. Add butter and shortening and process until homogenous dough just starts to collect in uneven clumps; dough will resemble cottage cheese curds and there should be no uncoated flour. Scrape bowl with rubber spatula and redistribute dough evenly around processor blade. Add remaining 5 oz. flour and pulse until mixture is evenly distributed around bowl and mass of dough has been broken up. Empty mixture into medium bowl.
Sprinkle vodka and water over mixture. With rubber spatula, use folding motion to mix, pressing down on dough until dough is slightly tacky and sticks together. Separate into two disks. Wrap each in plastic wrap and refrigerate at least 45 minutes or up to 2 days.

Filling: (makes enough for at least 20+ pies. However, the crust recipe makes only enough for 13-14 pies).
7¼ oz. brown sugar
½ tsp. salt
½ T. cinnamon
½ tsp. ginger
¼ tsp. cloves
⅜ tsp. nutmeg
15 oz. canned pumpkin (see note below)
1 c. heavy cream
2 eggs

Stick blend or run through the food processor until smooth.

Pour into an 8x8 glass pan (greased, but not heavily) in an 11x15 pan water bath. Bake at 350 for 45 minutes or until center reaches 180 degrees. Refrigerate filling.

A note on the pumpkin: We actually prefer home-cooked butternut squash. You can also use home-cooked pumpkin, but always make sure to drain off the extra liquid before weighing.

Optional Cream Cheese Filling:
6 oz. cream cheese
½ c. butter, browned (about 94g post-browning weight)    
½ tsp. vanilla  
375g powdered sugar

Beat browned butter and cream cheese until fluffy. Add vanilla and powdered sugar and beat until good and fluffy again. Refrigerate for at least two hours before using.

Assembly:
Roll both pie crusts into 12-13 inch rounds. Using a 4 1/2 inch cookie cutter (we use a sour cream container lid) cut out 13-14 pies (you will need to use the scraps to get this many). Allow the circles to firm up a bit in the refrigerator. When ready to assemble the pies, remove one disk at a time. Place between pieces of waxed paper, and roll out a bit more to a 5 1/4 inch circle.

Use a #60 ice cream scoop (just over 1 tablespoon) to place two scoops on the bottom center-ish area of pie crust circle. Use an offset spatula to spread the filling on the bottom half of the circle, leaving plenty of room on the edges to seal. Dip your finger in water and wet the edges all the way around, then (using the wax paper to help) fold the top half over the bottom half. Seal with your finger first, pushing the filling inwards as you do so, then crimp with the fork after that (making sure you stay far away from the filling with your fork).

If doing a cream cheese/pumpkin combo, spread one scoop of the filling as usual, then add a heaping teaspoon of the cream cheese filling (rolled into a log shape) on top of the pumpkin pie filling. Next spread on another scoop of pumpkin filling. Seal as usual.

Refrigerate completed pies loosely covered in parchment paper for a few hours.

Fry at 375 for 6 minutes. 

Carefully dump onto a paper towel covered plate and let set for just a little bit before heavily cinnamon and sugaring (⅔ c. sugar, 1 T. cinnamon). You can keep them fresh in a barely warm oven (170), while frying the others.
