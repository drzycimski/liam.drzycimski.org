---
layout: post
title: Haystacks
date: 2015-03-25 14:01:34 -0500
---
![alt](/content/images/2015/03/Untitled-1.png)
Mom created this recipe from memories of haystacks at family gatherings when she was younger. Although you could probably make these using chow mein noodles, we preferred using corn flakes. We also include marshmallows, which add more variety of texture (and some great flavor). One tip though:  while making these, add the marshmallows after mixing the melted mixture (butterscotch chips and peanut butter) into the corn flakes. If you don't wait, the marshmallows will melt.
#The Recipe

1 c. peanut Butter
1 (12 oz.) bag butterscotch chips
6 c. cornflakes
3 c. mini marshmallows

Melt peanut butter and butterscotch chips in the microwave. Mix cornflakes and peanut butter/butterscotch mixture in a large bowl. After the cornflakes are mostly coated, mix in the marshmallows. Make sure everything is coated. Then spread into a jellyroll pan and chill to harden. Break into pieces and store in an airtight container.
