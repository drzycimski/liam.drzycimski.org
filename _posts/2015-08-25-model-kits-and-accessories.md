---
layout: post
title: Model Kits and Accessories
date: 2015-08-25 14:01:11 -0500
---
Model Kits and Accessories for Sale

**Kits:**

USS Constitution by Revell 1:96 model $45
I have only unpacked and repacked this carefully. No pieces should be broken or missing (see picture below).

HMS Bounty by Revell 1:110 model $13
I have only glued the ship's boat together and painted the sails (see pictures below).

Man In Space Kit by AMT: $15
Gemini-Titan, Appollo-Saturn 1-B, Mercury-Altlas, Mercury-Redstone, and Apollo-Saturn V
Partially put together and two partially painted rockets with carpenters tape still attached (see pictures below).

**Accessory Bundle $17**
(We have used some of the paint in the containers, as well as some of the spray-on enamel).

*Paint:*

Black x3
White x2
Gray x1
Silver x1
Gold x1
Tan x1
Light Tan x1
Red x1
Brown x1
Rust x1

*Spray Enamel*

*Paint Thinner/Brush Cleaner* (2 containers unopened, 1 container 1/4 full)

*Plastic Cement* (4 tubes: 3 unopened, 1 half used)

*8 Micro Brushes and 1 paint brush*


**Airbrush Bundle**:

*Air Compressor for Air Brush* $45

*Airbrush* (free--in need of repair)

The air compressor for the airbrush has barely been used. However, the airbrush itself is in need of repair.
####HMS Bounty Boat
![alt](/content/images/2015/09/unnamed-2.jpg)
####HMS Bounty Painted Sails
![alt](/content/images/2015/09/unnamed-1.jpg)
####Rockets
![alt](/content/images/2015/09/unnamed.jpg)
####USS Constitution
![alt](/content/images/2015/09/85-5404-lg.jpg)
####HMS Bounty
![alt](/content/images/2015/09/80-5404-lg.jpg)
####Man in Space
![alt](/content/images/2015/09/51do0kY7e0L.jpg)
####Accessories
![alt](/content/images/2015/09/unnamed-4.jpg)
####Airbrush and Compressor
![alt](/content/images/2015/09/unnamed-3.jpg)
