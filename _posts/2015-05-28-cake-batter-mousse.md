---
layout: post
title: Cake Batter Mousse
date: 2015-05-28 15:49:16 -0500
---
Mom came up with this recipe while she was on a dairy-free diet. She was able to use a whipped topping that only had one minor dairy-based derivative in it. Mom had originally made this without the whipped topping, and it was so incredibly rich you would only want to eat a little bit. The whipped topping fixes that. This was actually dad's idea.

At first we thought that only our family would like this recipe, but once we started introducing it to friends of ours we were proved wrong. Pretty much anybody who tasted it liked it. This is a quick and easy dessert that allows you to enjoy cake batter without risking raw eggs.

1 yellow cake mix
Ingredients on the back of the box, minus the eggs
8 oz. tub whipped topping
20 broken up Oreos

Mix the cake mix and the ingredients on the back of the box (minus the eggs) in a mixer on medium speed until smooth. Fold in cookies and whipped topping. Do not fold too long or you will deflate the whipped topping. For best results, refrigerate overnight to soften the cookies and allow the mousse to firm up. ![alt](/content/images/2015/05/Screen-Shot-2015-05-21-at-11-04-25-AM.png)
