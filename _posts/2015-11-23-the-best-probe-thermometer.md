---
layout: post
title: The Best Probe Thermometer
date: 2015-11-23 15:38:27 -0600
---
![alt](/content/images/2015/11/61-FqXCdWsL-_SL1000_.jpg)
For several months now, we have had the ThermoWorks ChefAlarm. Although it hasn’t totally lived up to our expectations, it is much better than any other probe thermometers that we have tried.

The ChefAlarm is not your average probe thermometer. A probe thermometer is, or rather should be, easy to use. You just stick it in the meat and forget about it until your high temp alarm goes off. The ChefAlarm has a button for every function and is so easy to use you don’t even need to use the manual. You’ll understand why this is helpful if you’ve ever tried to navigate something less than user-friendly—like an old iPod Nano. High temperature alarms are extremely helpful, but this one even comes with an adjustable alarm volume. It is made even more unique by tolerating temperatures up to 572 degrees, which is nice when grilling. Accuracy is a problem with lesser thermometers. In the past, we have had thermometers that are only accurate within 5-10 degrees, but this one is accurate within one degree. It’s more expensive, but its accuracy and ease of use makes it worth it.

The ChefAlarm has proved useful in more ways than one. This summer and fall I have used it countless times when finishing off a smoked pork butt in the crockpot. Because of its high temp alarm, I’m usually able to just forget about it until the roast is done. Since it actually reads pretty fast, it can also be used it as an instant-read thermometer in a pinch. Also, we used to always use an electric deep fat fryer for our frying needs. It regulated the temp but didn’t have a very large capacity. After we got the ChefAlarm, we were able to start frying in an 8 qt. pot. This allowed us to fry a lot more at once, making it much more convenient.

There are some drawbacks to the ChefAlarm too, although there aren’t as many as some other thermometers have. Other probe thermometers have totally died when the probe died, but with the ChefAlarm, only the probe died, and that after a few months of heavy abuse. However, the ThermoWorks website isn’t very clear when telling you that it is normal to have to replace the probe every once and awhile. It would have been nice to at least have had another one on hand when this happened, so I recommend you buy a back-up. Another downside is that the ChefAlarm doesn’t come with a very good probe clip (the thing that keeps the probe in the pot when using it for frying or candy making), but you are able to buy a better one from ThermoWorks for a couple of dollars. Also, ThermoWorks tells you not to kink the probe’s cord, but at the same time they give you a case for the ChefAlarm that will obviously kink the cord. We just found another way to store it.

So is it worth its $59 price tag? We think so. Although it isn’t perfect, you at least don’t have to worry about it completely dying on you. Overall, it has been extremely helpful to have a reliable probe thermometer.

[Buy the ChefAlarm.](http://www.thermoworks.com/products/alarm/chefalarm.html) To buy an extra probe, select the Pro-Series Probes tab on ChefAlarm page. If you want to buy a better probe clip, select the accessories tab.
