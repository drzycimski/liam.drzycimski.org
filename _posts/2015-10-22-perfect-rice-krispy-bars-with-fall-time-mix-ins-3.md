---
layout: post
title: Perfect Rice Krispy Bars with Fall Time Mix-ins
date: 2015-10-22 10:09:18 -0500
---
![alt](/content/images/2015/10/DSC_0043.jpg)

Who needs to know any more about Rice Krispy bars? Really—aren’t they on the back of every single Rice Krispies box and bag of marshmallows? Although there is nothing horribly wrong with these recipes, we have a recipe that is not only innovative, but better than the average. Our Rice Krispies are softer, less dry, and last longer than others. We also gave them a flavor boost with some heavenly browning and good (though optional) add-ins.

We got the base for this recipe from a blog called [Cookies and Cups](http://cookiesandcups.com/marshmallow-fluff-krispie-treats/). The recipe on the blog starts just like the recipes on the back on the box except that it has some extra salt added. However, then the recipe also adds tons of marshmallow fluff in order to make the bars softer and give additional marshmallow flavor without making them too chewy. Whole marshmallows are added in at the the end for even more marshmallow goodness. We only changed this base recipe a bit by using browned butter instead of regular and toasted marshmallows instead of non-toasted. The ideas for these changes actually came from [another recipe](http://cookiesandcups.com/toasted-marshmallow-krispie-treats/) from the same blog. For our part, we added in some mix-ins that include peanuts (or pretzels), candy corn, and white chocolate chips, which are all optional but they make for a very nice mix of flavors. You can use other mix-ins, though we haven’t experimented with any others.

######Base Recipe
59g Browned Butter, melted (post browning weight—pre-browning measurement is about 5 T.)
½ tsp. salt
280g mini marshmallows (6c./10oz.)
180g Rice Krispies (6c.)
7 oz. container marshmallow fluff (1½ c.)
93g mini marshmallows (2c./3⅓ oz.)

######Optional Mix-ins
150g chopped peanuts (chopped in half) OR 4 oz. pretzel goldfish
200g candy corn—(chopped in half)
1 c. white chocolate chips

Line a 9x13 pan with foil. Spray lightly with cooking spray. Preheat broiler.

Melt browned butter in medium mixing bowl. Stir in the salt.

Line a 12x18 sheet pan with foil. Spray thoroughly with cooking spray. Sprinkle the 280g of marshmallows (6c./10oz.) over the surface of the pan in an even layer. Broil until puffed and *lightly*  toasted. (Watch carefully!). The whole shebang should slide off the foil nice and easily into your mixing bowl of butter/salt. Mix together. Add the Rice Krispies. Mix together. Add the fluff. Mix together. Add the remaining marshmallows, chopped peanuts, candy corn, and white chocolate chips. Mix thoroughly and press into the 9x13 pan. Press more compactly than typical Rice Krispy bars, but still be careful to not compress them too much. Cover with plastic wrap and allow to set up for at least an hour before cutting/serving (we typically go over night—these things will last beautifully for DAYS.)
