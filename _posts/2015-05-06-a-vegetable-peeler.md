---
layout: post
title: A Vegetable Peeler
date: 2015-05-06 16:35:34 -0500
---
For many years we had been using a peeler a lot like the [Faberware Euro Peeler](http://www.walmart.com/ip/Farberware-Euro-Peeler-Red/14964902 ). We had liked it well enough, but the blade eventually dulled. Eventually we saw the Kuhn Rikon Original Swiss Peeler on an equipment review on [America's Test Kitchen](http://www.americastestkitchen.com), and we ended up buying it. Although it looks cheap and like it will break the first time you use it, it is actually pretty sturdy. We have had a few for the last several months and they still haven't broken. Aside from being sturdy, these peelers have super sharp, carbon steel blades. The only downside to carbon steel is that it will rust if not hand-washed and dried immediately. Even though it looks like it will fit awkwardly in the hand, it is actually pretty comfortable. The cost is only $7.31 on Amazon, but we recommend the Kuhn Rikon 3-Set too since it is only $9.99 ($3.33 per peeler) compared to $7.31 per peeler.

The Kuhn Rikon 3-Set Original Swiss Peeler, Red/Green/Yellow [Buy Now](http://www.amazon.com/Kuhn-Rikon-Original-Peeler-Yellow/dp/B001BCFTWU/ref=sr_1_1?s=home-garden&ie=UTF8&qid=1430944806&sr=1-1&keywords=kuhn+rikon+3-set+original+swiss+peeler+red+green+yellow)
![alt](/content/images/2015/05/71HmQwWQVkL-_SL1500_.jpg)
