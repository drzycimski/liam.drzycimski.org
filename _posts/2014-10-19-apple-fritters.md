---
layout: post
title: Apple Fritters
date: 2014-10-19 15:24:45 -0500
---
This recipe is from Cooks Country. The only things we changed were the amount of glaze we had in the end and our frying method. The realy nice thing about these fritters is that they are almost better the next day. One disclaimer, do not store them in an airtight container! Instead, wrap a towel around them so that they can breathe.
##The Recipe

####Fritters:
2 c. diced apples, about ¼ inch pieces
2 c. all-purpose flour
1/3 c. granulated sugar
1 T. baking powder
1 tsp. salt
1 tsp. ground cinnamon
1/4 tsp. ground nutmeg
3/4 c. apple cider
2 eggs, lightly beaten
2 T. melted butter


####Glaze:
3 c. powdered sugar
3/8 c. apple cider (plus more?)
3/4 tsp. ground cinnamon
3/8 tsp. ground nutmeg

Spread diced apples in single layer on paper towel-lined baking sheet and pat thoroughly dry with paper towels (two layers on top, two layers on bottom).

Combine flour, sugar, baking powder, salt, cinnamon, and nutmeg in a large bowl. Whisk cider, egg, and melted butter in medium bowl. Stir apples into flour mixture. Stir in cider mixture until combined.

For the batter, we used our largest cookie scoop (2 T., but since we heaped it, it was closer to 3 T. probably). Plop the slightly heaping scoop of batter on to your fingers in your left hand, use a spatula to sort of flatten it out a bit, then slide the flattened mound off your fingers and into the fryer. Flip when needed—they didn’t have any problems staying flipped over, so you can cook one side completely, and they’ll still flip.

Drain on a wire rack on a baking sheet. (Foil-lined is a good idea as the frosting and grease drips will make a mess). After they have cooled a bit (3 minutes or so), dip the top half of the fritters into the bowl of glaze. Flip it back upright in your hand and hold it there for a few seconds while it warms up, then tip it upside down again and shake it a bit to get some of the excess off. Place it back on the rack.

After the top half glaze has had a few minutes to soak in and harden a bit, then do the bottom half in the same way, placing the fritters “bottom half up” when you put them back on the rack.
