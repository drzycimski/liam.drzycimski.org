---
layout: post
title: Roasted Cabbage
date: 2015-03-19 18:28:47 -0500
---
![alt](/content/images/2015/03/unnamed-2.jpg)
Since cabbage is cheap, we buy a lot of it when it is on sale. Mom ran across a recipe for roasted cabbage that included a small amount of sugar to help with browning. We had made roasted cabbage before, but we had never used sugar. We loved this technique so much that most of our cartful of cabbages will have the same fate.
#The Recipe

1 tsp. salt
1 tsp. sugar
Pepper to your taste
Cabbage (see amount below)
Cooking spray
Balsamic vinegar (optional)

Spray a foil-covered 12x18 baking sheet heavily with cooking spray. Adjust oven rack to bottom position, and heat oven to 550 degrees. Combine salt and sugar in small bowl. Quarter cabbage and remove core. Cut each quarter into 3/4-inch slices. Place as many as can fit without touching each other on the baking sheet (about 1 small head or 1/2 large head. Extra can be wrapped in plastic wrap and refrigerated for a few days). Spray top of cabbage slices with cooking spray and sprinkle with salt mixture and pepper (preferably fresh-ground).

Roast for 15-20 minutes or until cabbage is slightly blackened on the bottom. Turn oven onto broil and move cabbage to top rack. Broil for 2-3 minutes or until slightly blackened on top. Remove from oven, and if you like, drizzle with balsamic vinegar.
