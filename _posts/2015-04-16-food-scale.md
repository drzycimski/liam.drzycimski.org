---
layout: post
title: The Perfect Food Scale
date: 2015-04-16 11:53:45 -0500
---
After our old kitchen scale started to run down (it had lasted for quite a few years, and we wanted something as a backup with a higher weight capacity), we decided to buy the [EatSmart Precision Elite Digital Kitchen Scale](http://www.amazon.com/EatSmart-Precision-Elite-Digital-Kitchen/dp/B009EUPMFK/ref=sr). We ended up sending it back. We didn't like it because after you placed whatever you were weighing on the scale, the weight the scale gave you kept climbing.

Next, we bought the Baker's Math KD8000 Scale by My Weigh, which we had originally avoided because of how big it was. We ended up loving it for its accuracy. We also like how it has five weighing modes: kilograms, grams, ounces, pounds, and pounds/ounces. We like the option of being able to plug the scale in with an AC adapter. You can also program it so that it won’t automatically shut off (this is not in the owner’s manual included with the scale, but you can find it on page 3 of this [pdf](http://myweigh.com/resources/manuals/kd8000.pdf)), but we recommend that you don't leave it on this setting while it's running on battery power. It comes with a removable gunk shield for the LCD screen and buttons, but we don't use it that often. I love how it has a bright backlight for the LCD screen. This helps when you are weighing something in dim lighting. The weight only climbs 1 gram (if that) while measuring, compared to the 2-5 grams the EatSmart scale would climb.

The two biggest improvements over our old scale are the updated taring system and increased capacity. Although it is a feature on other scales as well, the updated taring system has really helped by showing negative values. If you place a container on the scale, tare it, and then take the container off, it will show a negative value on the LCD. This is the weight of the container. It will remember that weight (unless the scale shuts off) while you are busy with your ingredients. Another way you can use the negative values is by (for example) placing a bag of flour on the scale, taring it, and taking flour out of the bag and placing it in the bowl you are measuring into. If you needed 30g flour you will want to stop spooning flour out once the scale has the number -30 on the LCD. This is helpful when you’d rather not be measuring straight into your other ingredients.

The capacity is probably the biggest improvement over our old scale. It is actually 3.5 times higher than our old scale (approx. 17.5 lbs compared to 5 lbs). We love this because a five pound weight capacity just doesn’t cut it when you try to weigh a 10 lb bag of potatoes. This also helps when you are weighing something into a heavy cast iron skillet or a crock pot.

Overall we highly recommend this scale for anyone who wants to accurately follow recipes that include measurements by weight instead of volume.

The Baker's Math KD8000 Scale by My Weigh [Buy Now](http://amzn.to/1JT06iL)![alt](/content/images/2015/04/kd8000header1.jpg)
