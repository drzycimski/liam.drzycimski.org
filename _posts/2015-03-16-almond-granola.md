---
layout: post
title: Vanilla Nut Granola
date: 2015-03-16 15:54:13 -0500
---
We first ran across this recipe on a cooking show called America's Test Kitchen. Except for taking out some of the nuts (to make it less expensive) and adding some more salt, we stuck to the original recipe. This granola makes a great, though not totally healthy (think oil, sugar, and syrup) snack that will keep for at least a couple weeks if kept in an airtight container.

#The Recipe
1/3 c. maple syrup
80g light brown sugar (about 1/3 c.)
4 tsp. vanilla extract
3/4 tsp. salt
1/2 c. vegetable oil
500g rolled oats (about 5c.)
100g raw nuts (we use almonds and pecans), chopped coarsely

Whisk together the maple syrup, brown sugar, vanilla, salt, and oil. Add rolled oats and nuts. Stir well. Dump into a parchment-lined 18x12 jelly-roll pan and spread to the edges. Cover with waxed paper. Place a similar sized pan on top and press to flatten. Remove top pan and waxed paper. Bake at 325 degrees for 30 to 35 minutes, rotating halfway through. Oats should be just barely light golden in color, or "toasted but not toast." Allow to cool for 1 hour before breaking into pieces. Should last at least a week, probably two, and maybe longer, but we’ve never managed to keep it around that long. :-)
