---
title: Spinach Artichoke Dip
date: 2015-06-01 14:11:43 -0500
published: false
---
This rich and creamy dip is a perfect side for a gathering in either summer or winter. It is a little spicy, but not overpoweringly, and it is best served warm on crackers or as a pizza sauce.
#The Recipe
1 1/2 oz. frozen spinach
3 oz. drained artichoke hearts
4 oz. cream cheese
2 oz. sour cream
1 oz. mayo
3/4 oz. shredded parmesan (Do Not Use Powdered!)
SCANT 1/4 tsp. salt
SCANT 1/4 tsp. garlic
1/4 tsp. red pepper flakes (level)

Thaw spinach, and squeeze it out with cheesecloth. 
Boil until tender in 1/2 c. water (make sure you chop artichokes while boiling). Drain off the water. Over low heat, mix in cream cheese, sour cream, mayo, parmesan, salt, garlic, and red pepper flakes. Cook until melted smooth.
![alt](/content/images/2015/08/Screen-Shot-2015-08-27-at-4-02-19-PM.png)