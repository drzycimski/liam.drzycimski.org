---
title: Ramen Salad Crunchies
date: 2015-05-21 06:16:15 -0500
published: false
---
These were originaly from an Asian style salad, but they can be used in any other type of salad. This recipe used to have you totaly melting the butter. This ment that the butter would pool in some places more than others on the pan, thus creating uneven browning. We remedied this by only softening the butter until it was the consistency of mayonaise

3 pkg ramen noodles
3/4 c. almonds
1 1/2 T. sesame seeds
6 T. butter, softened to the consistency of mayonaise

Mix all ingredients together and dump onto a foil-lined 12x18 baking sheet. Cook for 8-10 minutes at 350 degrees. Stir halfway through. They should be golden brown. Let cool, and store in a Ziplock bag in the refrigerater.

![alt](/content/images/2015/08/Screen-Shot-2015-08-27-at-4-02-19-PM-1.png)