---
title: Pasta Salad
date: 2015-05-20 08:39:51 -0500
published: false
---
We first tasted this salad at a party, and we really liked it because it was better than most Italian pasta salads. After getting the recipe, we changed it, but still kept the idea of using Italian *and* ranch dressing, which is what makes it different from other pasta salads. This is a great pasta salad for summertime family gatherings.

24 oz. tri-color rotini pasta
12 oz. Olive Garden dressing*
12 oz. Ranch dressing (we use Kraft's 3 Cheese Ranch) 
2 oz. pepper jack, shredded
2 oz. Cheddar, shredded
Parmesan, olives, tomatoes, and mini pepperonis to your taste.

Cook pasta according to instructions until al dente. While pasta is cooking combine ranch and Olive Garden dressing. Once pasta is cooked, drain it and rinse with cold water. Combine pasta and dressing. Let set in the refrigerator until the day before serving. Then add pepper jack, cheddar, and whatever you would like of parmesan, olives, tomatoes, and mini pepperonis. Place it in the fridge overnight. Extra dressing may need added before serving.

*Can be substituted with any other Italian dressing, but we prefer Olive Garden dressing.
![alt](/content/images/2015/08/Screen-Shot-2015-08-27-at-4-02-19-PM-2.png)